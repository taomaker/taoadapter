# TaoAdapter

This is a collection of adapters for using old lens on mirrorless cameras designed by me (Aaron Zhang). These adapters can be 3D printed or machined.

### Rodenstock Reomar 38mm f/2.8 from Kodak Instmatic 250

### Carl Zeiss Tessar 40mm f/2.8 from Rolleiflex SL26

### Color-Skopar 50mm f/2.8 from Voigtlander Vito Automatic

### Color-Pantar 45mm f/2.8 from Zeiss Ikon Contessamat SE
